This repo aims to reproduce AE experiments using mafaulda database.

The experiments were done using Python 3.11.
Install the requirements in requirements.txt by: pip install -r requirements.txt

Run "extract_mafaulda_sound.py" to create .wav files from machine audio from mafaulda dataset.

Run "feature_extraction_mel.py" to crate Mel spectrogram features from the .wav files generated before.
This program will create a folder with binaries with the mel features.

Run "make_sound_feature_dataset_table.py" to create a .csv table that references each binary.

Run "sound_mel_ae.py" to train an AE on the mel spectrogram. This program will perform a 5x5 nested cross-validation.
It will rely on the .csv table formely generated.

Run "sound_mel_nn.py" to train an NN on the mel spectrogram. This program will perform a 5x5 nested cross-validation.
It will rely on the .csv table formely generated.

Run "make_feature_dataset.py" to create a .csv feature dataset relyied on statistical and spectral attributes.

Run "stat_nn.py" to train a simple NN on those attributes. You can switch in line 263 and 264 between using the attributes from all sensors or only accoustic. This program also performs a 5x5 nested cross-validation.

Run "eval_nn_testset.py" to evaluate your models. Check the code to customize according to what you want to evaluate.
